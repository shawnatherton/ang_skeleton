# README #

The purpose of this kit is to provide the user with a foundation to create angular 2 apps. It is a skeleton. You will of course want to run yeoman to create a nice little directory structure for your app for more info on yeoman go to http://yeoman.io

npm install -g grunt-cli bower yo generator-karma generator-angular

I didn't include those modules in the package.json because maybe you don't want them.

### What is this repository for? ###

* Starter Kit for Angular projects
* Version 1


### How do I get set up? ###

* First clone the repo to the directory of choice. 
* Second run: npm install
* Third create some html and angular
* Fourth serve it up with the command: npm start
* How to run tests
* Deployment instructions